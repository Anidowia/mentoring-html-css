### Name: Dina Sherubayeva

### Achievements:

1. Golden medal school alumni
2. Top 1 Technical Kz University student
3. EPAM Tech Orda program participant

### Other:

- Academic mobility program alumni (Poland - Kazakhstan)
- Know Russian, Kazakh, English, Chinese, German, Polish
- GPA 3.7/4
- Writing an university full - stack web app project
